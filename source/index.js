const blessed = require('blessed')
const screen = blessed.screen({
  smartCSR: true,
  autoPadding: false,
  warnings: false,
  dockBorders: true,
  title: "lummo.space"
});

const Socket = require('socket.io-client')
const SocketEndpoint = 'https://ws.lummo.space'
const SocketRoom = process.argv[2] || 'lobby'
const SocketUser = process.argv[3] || 'slaughter'
const IO = Socket(`${SocketEndpoint}/${SocketRoom}`, {'transports': ['websocket', 'polling']})
var useras = []
IO.emit('user join', {
  user: SocketUser
})

IO.emit('get history')
IO.emit('get users')

const parse = require('parse-mentions')

var logger = blessed.log({ 
  parent: screen,
  mouse: true,
  width: '72%',
  height: '85%',
  border: 'line',
  tags: true,
  keys: true,
  vi: true,
  mouse: true,
  scrollback: 100,
  scrollbar: {
    ch: ' ',
    track: {
      bg: 'yellow'
    },
    style: {
      inverse: true
    }
  }
});

const users = blessed.box({
  parent: screen,
  scrollable: true,
  mouse: true,
  left: "70%",
  height: "85%",
  width: "31%",
  border: 'line',
  tags: true,
  keys: true,
  vi: true,
  mouse: true,
  scrollback: 100,
  scrollbar: {
    ch: ' ',
    track: {
      bg: 'yellow'
    },
    style: {
      inverse: true
    }
  }
})

const box = blessed.box({
  parent: screen,
  mouse: true,
  top: '85%',
  width: '100%',
  height: '15%',
  border: 'line',
})

const text = blessed.textbox({
  parent: box,
  mouse: true,
  keys: true,
  top: 1,
  left: 1,
  width: "95%",
  height: 1,
  style: {
  }
})

text.on('focus', function() {
  text.readInput();
});

text.on('submit', (data) => {
  if (data === '') return
  if (data === '/clear') {
    logger = blessed.log({ 
      parent: screen,
      mouse: true,
      width: '70%',
      height: '85%',
      border: 'line',
      tags: true,
      keys: true,
      vi: true,
      mouse: true,
      scrollback: 100,
      scrollbar: {
        ch: ' ',
        track: {
          bg: 'yellow'
        },
        style: {
          inverse: true
        }
      }
    });

    text.focus()
    text.clearValue()
    logger.log(`{center}screen successfully cleared{/center}`)
    screen.render()
    return
  }

  if (data === '/clear history' && SocketUser === "slaughter") {
    IO.emit('clear history')
    logger = blessed.log({ 
      parent: screen,
      mouse: true,
      width: '70%',
      height: '85%',
      border: 'line',
      tags: true,
      keys: true,
      vi: true,
      mouse: true,
      scrollback: 100,
      scrollbar: {
        ch: ' ',
        track: {
          bg: 'yellow'
        },
        style: {
          inverse: true
        }
      }
    });
    
    text.focus()
    text.clearValue()
    logger.log(`{center}history successfully cleared{/center}`)
    screen.render()
    return
  }
  
  IO.emit('new message', {
    content: data
  })
  logger.log(`{blue-fg}${SocketUser}: {/blue-fg}${parse(data, replace()).output}`)
  text.focus()
  text.clearValue()
  screen.render();
})

IO.on('message history', (data) => {
  for (let message of data) {
    logger.log(`{blue-fg}${message.user}: {/blue-fg}${parse(message.content, replace()).output}`)
  }
  logger.setScrollPerc(100)
  screen.render();
})

IO.on('user list', (data) => {
  useras = []
  for (let user of data.names) {
    useras.push(user)
  }
  users.setContent(useras.join("\n"))
  screen.render();
})

IO.on('new message', (data) => {
  logger.log(`{blue-fg}${data.user}: {/blue-fg}${parse(data.content, replace()).output}`)
  screen.render();
})

IO.on('user joined', (data) => {
  logger.log(`{center}user {blue-fg}${data.user}{/blue-fg} joined{/center}`)
  IO.emit('get users')
})

IO.on('user left', (data) => {
  logger.log(`{center}user {blue-fg}${data.user}{/blue-fg} left{/center}`)
  IO.emit('get users')
})

screen.key(['C-c', 'escape'], function() {
  return process.exit(0)
});

text.focus()
screen.render();

function replace() {
  return function(tok, tokens) {
    if (tok.name === SocketUser) {
      var val = `{inverse}{yellow-fg}${tok.name}{/yellow-fg}{/inverse}`
    } else {
      var val = `{yellow-fg}${tok.name}{/yellow-fg}`
    }

    tokens.output = tokens.output.slice(0, tok.match.index) + val
      + tokens.output.slice(tok.match.index + tok.match[0].length);
    return val;
  };
}
